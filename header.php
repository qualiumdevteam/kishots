<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kishots
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="large-12 columns site-header" role="banner">
		<a class="menu-button"></a>
		<div class="medium-2 large-2 columns site-branding text-center">
			<a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/logokishots.png"></a>
		</div><!-- .site-branding -->
		<nav id="site-navigation" class="medium-7 large-7 columns main-navigation text-center" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
		<div class="medium-3 large-3 columns redes text-center">
			<div class="iconos">
				<div class="glyph-icon flaticon-facebook3"></div>
				<div class="glyph-icon flaticon-logo22"></div>
				<div class="glyph-icon flaticon-instagram19"></div>
			</div>
		</div>
		<a class="iconredes" href="#"><img src="<?php echo get_template_directory_uri() ?>/img/share.png"></a>

	</header><!-- #masthead -->

	<div id="content" class="large-12 columns site-content">
