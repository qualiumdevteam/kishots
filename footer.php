<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kishots
 */

?>

	</div><!-- #content -->
<div class="overlay-footer"></div>
<div class="clearfix"></div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<p>Derechos Reservados Ki’ Shots© 2015</p>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
