jQuery(document).ready(function() {
    jQuery('.carrusel_paquetes').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        navText: ['<div class="prev"></div>','<div class="next"></div>'],
        dots: false,
        URLhashListener: true,
        startPosition: 'URLHash',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });


    jQuery('.carrusel_productos').owlCarousel({
        loop: true,
        center: true,
        items:2,
        margin: 10,
        nav: true,
        navText: ['<div class="prev"></div>','<div class="next"></div>'],
        dots: false,
        URLhashListener: true,
        startPosition: 'URLHash',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

    jQuery('.iconredes').click(function(){
       jQuery('.redes').toggleClass('mostrar');
    })

    jQuery(window).resize(function(){
        var ancho = jQuery('body').width();
        if(ancho>840){
            jQuery('.menu-button').removeClass('active');
            jQuery('.menu-button').removeClass('visible');
            jQuery('.main-navigation').removeClass('visible');
            jQuery('.overlay-footer').removeClass('activo');
        }
    })

    jQuery('.menu-button').click(function(){
        jQuery(this).toggleClass('active');
        jQuery(this).toggleClass('visible');
        jQuery('.main-navigation').toggleClass('visible');
        jQuery('.overlay-footer').toggleClass('activo');
    })

    /*if(jQuery('body').hasClass('home')){
        var items=jQuery('.item');
        var itemsvisibles=5;
        var tamano=100/itemsvisibles;
        for (var i=0; i <= items.length; i++){
            if(itemsvisibles > i) {

                if(i==0){
                    jQuery(items[i]).addClass('left1');
                }

                if(i==1){
                    jQuery(items[i]).addClass('left2');
                }

                if(i==2){
                    jQuery(items[i]).addClass('center');
                }

                if(i==3){
                    jQuery(items[i]).addClass('right1');
                }

                if(i==4){
                    jQuery(items[i]).addClass('right2');
                }

                jQuery(items[i]).addClass('visible');
            }
            else{
                jQuery(items[i]).addClass('invisible');
            }
        }

        var dataimg_left1 = 0;
        var dataimg_left2 = 1;
        var dataimg_Cen = 2;
        var dataimg_right1 = 3;
        var dataimg_right2 = 4;

        jQuery('.navleft').click(function(){

            if (dataimg_Cen == 0) {
                dataimg_left1 = items.length-1;
            }else{
                dataimg_left1 = dataimg_Cen - 1;
            };

            if (dataimg_left1 == 0) {
                dataimg_left2 = items.length-1;
            }else{
                dataimg_left2 = dataimg_left1 - 1;
            };

            console.log(dataimg_left1);
            console.log(dataimg_left2);
            console.log(dataimg_Cen);

            img_left1 = jQuery(items[dataimg_left1]);
            img_left1.addClass('animateleft2');
            img_left1.removeClass("left1 ");
            img_left1.addClass("left2");

            img_left2 = jQuery(items[dataimg_left2]);
            img_left2.addClass('animatecentert');
            img_left2.removeClass("left2");
            img_left2.addClass("center");

            img_center = jQuery(items[dataimg_Cen]);
            img_center.addClass('animateright1');
            img_center.removeClass("center");
            img_center.addClass("right1");


        })
    }*/

    var primeragal=jQuery('.imggaleria');
    jQuery(primeragal[0]).parents().addClass('activo');
    var src=jQuery(primeragal[0]).data('src');
    jQuery('.imggrande').css('background','url('+src+')');

    jQuery('.imggaleria').click(function(){
        jQuery('.imggaleria').parents().removeClass('activo');
        jQuery(this).parents().addClass('activo');
        var srcimagen=jQuery(this).data('src');
        jQuery('.imggrande').css('background','url('+srcimagen+')');
    })

})
