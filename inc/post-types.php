<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
        register_post_type('Paquetes',
        array(
            'labels' => array(
                'name' => __('Paquetes'),
                'singular_name' => __('paquetes')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );

    register_post_type('Productos',
        array(
            'labels' => array(
                'name' => __('Productos'),
                'singular_name' => __('productos')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );

    register_post_type('Galeria',
        array(
            'labels' => array(
                'name' => __('Galeria'),
                'singular_name' => __('galeria')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );
}