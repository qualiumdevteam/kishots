<?php
/*
* Template Name: Nosotros
*/
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<section style="background-image: url('<?php echo $feat_image; ?>')" class="nosotros">
<div class="overlay"></div>
    <h2 class="titulo">Nosotros</h2>
    <div class="contenido">
    <?php while(have_posts()) : the_post(); ?>
        <?php echo the_content(); ?>
    <?php endwhile; ?>
    </div>
</section>
<?php get_footer(); ?>
