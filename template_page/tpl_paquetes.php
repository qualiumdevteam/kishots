<?php
/*
* Template Name: Paquetes
*/
get_header();
$args = array(
    'post_type' => 'paquetes',
    'posts_per_page' => -1,
    'order' => 'DESC'
);
$query = new WP_Query($args);
$portada = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<section style="background-image: url('<?php echo $portada ?>')" class="paquetes">
   <div class="overlay"></div>
    <div class="carrusel_paquetes">
        <?php $contador=1; ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
            <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
            <div data-hash="<?php echo $query->post->post_name; ?>" class="item paqueteitem">
                <h4 class="text-center titulo_paquetes"><?php echo get_the_title(); ?></h4>
                <div style="background-image: url('<?php echo $feat_image; ?>')" class="text-center imgpaguete"></div>
                <div class="descripcion">
                    <?php the_content(); ?>
                </div>
                <a href=""><div class="btn_solicitar">solicitar</div></a>
            </div>
            <?php $contador++; ?>
        <?php endwhile; ?>
    </div>
</section>
<?php get_footer(); ?>
