<?php
/*
* Template Name: Shots
*/
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<section class="shot">
    <h2 class="titulo">Shots</h2>
    <div class="contenido">
    <div style="background-image: url(<?php echo $feat_image; ?>)" class="imgportda"></div>
        <?php while(have_posts()) : the_post(); ?>
            <?php echo the_content(); ?>
        <?php endwhile; ?>
        <a href=""><div class="btn_solicitar">solicitar</div></a>
    </div>
</section>
<?php get_footer(); ?>
