<?php
/*
* Template Name: Productos
*/
get_header();
?>
<section class="productos">
    <div class="overlay"></div>
    <h2 class="titulo">Productos</h2>
    <div class="contenido">
        <div class="small-12 medium-6 large-6 columns shots">
            <div class="text-center portada">
                <a href="<?php echo site_url() ?>/shots"><img src="<?php echo get_template_directory_uri() ?>/img/1.jpg"></a>
                <p>Shots</p>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet efficitur ipsum. Nam turpis metus, lacinia id imperdiet id, tincidunt vitae quam. Nullam et ante vitae lectus vestibulum euismod. Curabitur eu erat et felis interdum imperdiet vel sit amet odio. Ut at faucibus nunc. Donec id metus semper, finibus purus in, euismod arcu. Curabitur fringilla condimentum felis, in consectetur libero lobortis sit amet.</p>
        </div>
        <div class="small-12 medium-6 large-6 columns jelly_shots">
            <div class="text-center portada">
                <a href="<?php echo site_url() ?>/jelly-shot"><img src="<?php echo get_template_directory_uri() ?>/img/2.jpg"></a>
                <p>Jelly shots</p>
            </div>
            <p>Duis a feugiat arcu. Sed mauris nisl, fermentum id pretium et, pharetra nec odio. Proin eleifend arcu mollis molestie venenatis. Vestibulum eget odio sit amet lorem rutrum auctor. Donec at justo lacus. Donec maximus neque et dignissim ultrices. Maecenas eu leo commodo, convallis risus vitae, convallis metus. Nulla tempus dolor nec elit vulputate tempor.</p>
        </div>
        <div class="clearfix"></div>
        <a href=""><div class="solicitar">Solicitar</div></a
    </div>
</section>
<?php get_footer(); ?>
