<?php
/*
* Template Name: Contacto
*/
get_header();
$portada = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));
?>
<script>
    function initialize() {
        var coordenadas = new google.maps.LatLng(20.979379, -89.6227589);
        var coordenadas2 = new google.maps.LatLng(20.979379, -89.622758);
        var colores = [
            {
                featureType: "all",
                elementType: "all",
                stylers: [
                    {saturation: -100}
                ]
            }
        ];
        var opciones = {
            zoom: 16,
            center: coordenadas,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: false,
            scaleControl: false
        };
        var mapa = new google.maps.Map(document.getElementById('pony'), opciones);
        var estilo = new google.maps.StyledMapType(colores);
        mapa.mapTypes.set('mapa-bn', estilo);
        mapa.setMapTypeId('mapa-bn');
        var marcador = new google.maps.Marker({
            position: coordenadas2,
            map: mapa
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<section style="background-image: url('<?php echo $portada; ?>')" class="contacto">
    <div class="overlay"></div>
    <h2 class="titulo">Contacto</h2>
    <div class="contenido">
        <div class="datos_contacto small-12 medium-6 large-6 columns">
            <?php while(have_posts()) : the_post(); ?>
                <?php echo the_content(); ?>
            <?php endwhile; ?>
            <div id="pony"></div>
        </div>
        <div class="form_contacto small-12 medium-6 large-6 columns">
            <?php echo do_shortcode('[contact-form-7 id="25" title="Formulario de contacto 1"]'); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>
