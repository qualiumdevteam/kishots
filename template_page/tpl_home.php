<?php
/*
* Template Name: Home
*/
get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'paquetes',
    'posts_per_page' => -1,
    'order' => 'DESC'
);
$query = new WP_Query($args);


$args_galeria = array(
    'post_type' => 'galeria',
    'posts_per_page' => -1,
    'order' => 'DESC'
);
$query_galeria = new WP_Query($args_galeria);


$args_productos = array(
    'post_type' => 'productos',
    'posts_per_page' => -1,
    'order' => 'DESC'
);
$query_productos = new WP_Query($args_productos);
?>
<script>
    function initialize() {
        var coordenadas = new google.maps.LatLng(20.979379, -89.6227589);
        var coordenadas2 = new google.maps.LatLng(20.979379, -89.622758);
        var colores = [
            {
                featureType: "all",
                elementType: "all",
                stylers: [
                    {saturation: -100}
                ]
            }
        ];
        var opciones = {
            zoom: 16,
            center: coordenadas,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: false,
            scaleControl: false
        };
        var mapa = new google.maps.Map(document.getElementById('pony'), opciones);
        var estilo = new google.maps.StyledMapType(colores);
        mapa.mapTypes.set('mapa-bn', estilo);
        mapa.setMapTypeId('mapa-bn');
        var marcador = new google.maps.Marker({
            position: coordenadas2,
            map: mapa
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<section style="background-image: url('<?php echo $feat_image; ?>')" class="portada">
    <div class="overlay"></div>
    <div class="center">
        <h2 class="titulos">haz de tu evento</h2>
        <h2 class="titulos">algo diferente</h2>
        <div class="contenido">
        <?php while(have_posts()) : the_post(); ?>
            <?php echo the_content(); ?>
        <?php endwhile; ?>
        </div>
        <a class="" href="#"><div class="text-center more_information">Más información</div></a>
    </div>
    <div class="down"><a href=""><img src="<?php echo get_template_directory_uri() ?>/img/down.png"></a></div>
</section>
<section class="paquetes_home">
    <div class="overlay"></div>
    <div class="contenedor_info">
        <h2 class="titulo">paquetes</h2>
        <div class="divisor"></div>
        <?php $contador=1; ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
            <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
            <div class="paquete small-12 medium-6 large-3 columns">
                <a href="<?php site_url() ?>/kishots/paquetes/#<?php echo $query->post->post_name; ?>"><div style="background-image: url('<?php echo $feat_image; ?>')" class="imgpaguete"></div></a>
                <a href="<?php site_url() ?>/kishots/paquetes/#<?php echo $query->post->post_name; ?>"><h4 class="text-center titulo_paquetes"><?php echo get_the_title(); ?></h4></a>
            </div>
            <?php $contador++; ?>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
    </div>
    <div class="clear"></div>
</section>
<section class="paquetesmasinfo">
    <a href=""><div class="btnmore">Estoy interesado</div></a>
</section>
<section class="productos_home">
    <h2 class="titulo">productos</h2>
    <div class="divisor"></div>
    <div class="carrusel_productos contenido">
    <?php while($query_productos->have_posts()) : $query_productos->the_post(); ?>
        <?php $feat_image_productos = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
        <div class="item">
            <div style="background-image: url('<?php echo $feat_image_productos ?>')" class="portada_producto"></div>
            <h4 class="titulo_shot"><?php echo get_the_title(); ?></h4>
            <p><?php echo strip_tags(get_the_content()); ?></p>
        </div>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
    </div>
</section>
<section class="galeria_home">
    <h2 class="titulo">Galeria</h2>
    <div class="divisor"></div>
    <div dir="rtl" class="contenido">
    <div class="small-12 medium-3 large-3 columns contenedor_gal">
        <?php $contador=1; ?>
        <?php while($query_galeria->have_posts()) : $query_galeria->the_post(); ?>
            <?php $feat_image_galeria = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
            <div class="itemgal">
                <div class="overlay"></div>
                <a href="#"><div data-src="<?php echo $feat_image_galeria; ?>" style="background-image: url('<?php echo $feat_image_galeria; ?>')" class="imggaleria"></div></a>
            </div>
            <?php $contador++; ?>
        <?php endwhile; ?>
    </div>
    <div class="small-12 medium-9 large-9 columns imggrande"></div>
    </div>
</section>
<section class="contacto_home">
    <div class="info_contactohome small-12 medium-6 large-6 columns">
        <h2 class="titulo">contacto</h2>
        <div class="divisor"></div>
        <a href=""><div class="btnbotoncontactohome">llenar formulario</div></a>
    </div>
    <div id="pony" class="mapacontacto small-12 medium-6 large-6 columns">

    </div>
</section>
<section class="ordenashots">
    <div class="overlay"></div>
    <h2 class="titulo">SHOTS PARA TU FIESTA Y MÁS</h2>
    <a style="position: inherit;" href=""><div class="btnordenatusshots">Ordena tus shots</div></a>
</section>
<?php get_footer(); ?>
