<?php
/*
* Template Name: Galeria
*/
get_header();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'galeria',
    'posts_per_page' => -1,
    'order' => 'DESC'
);
$query = new WP_Query($args);
?>
<section class="galeria">
    <h2 class="titulo">Galeria</h2>
    <div class="contenido">
        <div class="small-12 medium-9 large-9 columns imggrande"></div>
        <div class="small-12 medium-3 large-3 columns contenedor_gal">
        <?php $contador=1; ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
            <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
            <div class="itemgal">
                <div class="overlay"></div>
                <a href="#"><div data-src="<?php echo $feat_image; ?>" style="background-image: url('<?php echo $feat_image; ?>')" class="imggaleria"></div></a>
            </div>
            <?php $contador++; ?>
        <?php endwhile; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>
